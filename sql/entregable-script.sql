-- Usuarios (#id, nombre, apellidos, direccion, contrasena, email, rol)
-- Restaurantes (#id, nombre, direccion, capacidad_maxima, -id_usuarios)
-- Reserva (#id, dia, hora, tiempo_estimado, dni_asistentes, nombre_asistentes, apellidos_asistentes)
-- Reserva_Restaurantes (#(id_reserva, id_restaurantes))
-- Reserva_Usuarios (#(id_reservas, id_usuarios)) 

USE entrega_sql;

SET FOREIGN_KEY_CHECKS = 0; 

CREATE TABLE IF NOT EXISTS usuarios (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    direccion VARCHAR(500),
    contrasena VARCHAR(50) NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL,
    rol VARCHAR (15)
);

CREATE TABLE IF NOT EXISTS restaurantes (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR (50),
    direccion VARCHAR (500),
    capacidad_maxima VARCHAR (15),
    id_usuarios INT UNSIGNED,
    FOREIGN KEY (id_usuarios) REFERENCES usuarios(id)
);

CREATE TABLE IF NOT EXISTS reservas (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    dia DATE,
    hora TIME,
    tiempo_estimado_de_estancia TIME,
    dni_asistentes VARCHAR (9),
    nombre_asistentes VARCHAR (50),
    apellidos_asistentes VARCHAR (50)
);

CREATE TABLE IF NOT EXISTS reserva_restaurantes (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_reservas INT UNSIGNED,
    FOREIGN KEY (id_reservas) REFERENCES reservas(id),
    id_restaurantes INT UNSIGNED,
    FOREIGN KEY (id_restaurantes) REFERENCES restaurantes(id)
);

CREATE TABLE IF NOT EXISTS reserva_usuarios (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    id_reservas INT UNSIGNED,
    FOREIGN KEY (id_reservas) REFERENCES reservas(id),
    id_usuarios INT UNSIGNED,
    FOREIGN KEY (id_usuarios) REFERENCES usuarios(id)
);

SET FOREIGN_KEY_CHECKS = 1; 