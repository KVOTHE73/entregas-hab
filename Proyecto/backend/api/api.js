//UTILIDADES DEL TOKEN
const config = require('./config')
const jwt = require('jsonwebtoken')

//LIBRERIAS
const database = require("../database.js");
const express = require('express');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const moment = require('moment');
const cors = require('cors')
const bodyparser = require('body-parser')
const nodemailer = require("nodemailer")
const mysql = require('mysql')
const app = express()

//APP USE
app.use(cors())
app.use(bodyparser.urlencoded ( { extended: true } ) )
app.use(bodyparser.json())
//importamos la llave
app.set('llave', config.llave)

//CONEXION A LA BBDD
const connection = mysql.createConnection( {
    host: "localhost",
    user: "proyecto",
    password: "1234",
    database: "proyect_hab_database",
})

connection.connect(error => {
    if(error) throw error
    console.log('Database up!⏫');
})

//PUERTO DEL SERVIDOR
const PORT = 3000

app.listen(PORT, () => console.log('Api up!🙃'));

//ENDPOINTS:

//RECOGER TODOS LOS USUARIOS DE LA BBDD

    //funciones - funcion para cargar un perfil de un usuario registrado
    const loadUsers = async (req, res) => {
    //SECUENCIA SQL
    const sql =
      "SELECT id, imagen, nombre, apellidos, dni, email, direccion, ciudad, provincia, codigo_postal, pais, telefono, rol FROM usuarios";
    try {
      const connection = await database.connection();
      const [rows] = await connection.execute(sql,(error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
            console.log('No existen actividades que mostrar');
        }
    })
    
    } catch (e) {
      return {
        code: 404,
        description: "Los parametros introducidos son incorrectos o inexistentes",
      };
    };
  };
app.get('/usuarios', loadUsers);  

//CARGAR PERFIL DE UN USUARIO
app.get('/usuarios/load/:id', (req, res) => {

    //DATOS QUE RECIBIMOS
    const id = req.params.id
    console.log(id);
    //SECUENCIA SQL
    const sql = `SELECT * FROM usuarios WHERE id='${id}'`;  

    //CONEXION A LA BBDD
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
        console.log('Perfil de Usuario cargado con exito');
    }
})
console.log(id);
})

//CARGAR LAS RESERVAS DE UN USUARIO
app.get('/usuarios/reservas/:id', (req, res) => {

    //DATOS QUE RECIBIMOS
    const id_usuario = req.params.id
    
    //SECUENCIA SQL
    const sql = `SELECT * FROM reservas WHERE id_usuario='${id_usuario}'`;  

    //CONEXION A LA BBDD
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
        console.log('Perfil de Usuario cargado con exito');
    }
})
})

//RECOGER TODAS LAS ACTIVIDADES DE LA BBDD
app.get('/actividades', (req, res) => {

    //SECUENCIA SQL
    const sql = 'SELECT * FROM actividades'

    //CONEXION A LA BBDD
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
            console.log('No existen actividades que mostrar');
        }
    })
})

//RECOGER TODAS LAS VALORACIONES DE LA BBDD
app.get('/ratings', (req, res) => {

    //SECUENCIA SQL
    const sql = 'SELECT * FROM rating'

    //CONEXION A LA BBDD
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
            console.log('No existen valoraciones que mostrar');
        }
    })
})

//RECOGER TODAS LAS RESERVAS DE LA BBDD
app.get('/bookings', (req, res) => {

    //SECUENCIA SQL
    const sql = 'SELECT * FROM reservas'

    //CONEXION A LA BBDD
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
            console.log('No existen reservas que mostrar');
        }
    })
})

//FILTRAR ACTIVIDADES POR FECHA
app.get("/actividades/date/:date", async (req, res) => {
  
    //DATOS QUE RECIBIMOS
    const date = req.params.date;
    const fecha = moment(date).format('YYYY/MM/DD');
    console.log(req.params.date);
    console.log(fecha);
    //VARIABLES
    let count = 0;
    let parameters = [];
    //SECUENCIA SQL
    let sql = "SELECT * FROM actividades"
    
    if (fecha) {
      if (count === 0) {
        sql += ' WHERE ';
      } else {
        sql += ' AND ';
      }
      sql += `fecha_inicio <= '${fecha}'`;
      parameters.push(fecha);
      count++;
    };
  
    if (fecha) {
      if (count === 0) {
        sql += ' WHERE ';
      } else {
        sql += ' AND ';
      }
      sql += `fecha_fin >= '${fecha}'`;
      parameters.push(fecha);
      count++;
    };
  
    console.log(sql);
    
    //CONEXION A LA BBDD
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, parameters);
        console.log(rows);
        res.json(rows)
                   
    } catch (e) {
          return {
            code: 404,
            description: "Los parametros introducidos son incorrectos o inexistentes",
          };
        };
    
});

//FILTRAR RESERVAS POR FECHA
app.get("/reservas/date/:date", async (req, res) => {
  
    //DATOS QUE RECIBIMOS
    const date = req.params.date;
    const fecha = moment(date).format('YYYY/MM/DD');
    console.log(req.params.date);
    console.log(fecha);
    //VARIABLES
    let parameters = [];
    //SECUENCIA SQL
    let sql = `SELECT * FROM reservas WHERE fecha='${fecha}'`;
    
    
    console.log(sql);
    
    //CONEXION A LA BBDD
    try {
        const connection = await database.connection();
        const [rows] = await connection.execute(sql, parameters);
        console.log(rows);
        res.json(rows)

    } catch (e) {
          return {
            code: 404,
            description: "Los parametros introducidos son incorrectos o inexistentes",
          };
        };
    
});

//AÑADIR USUARIOS A LA BBDD

//FUNCION DE REGISTRO
app.post('/usuarios/add', async (req, res) => {
    //HASHEAMNOS LA CONTRASEÑA
    const password = req.body.contrasenna;
    const encryptedPassword = await bcrypt.hash(password, saltRounds)
    //SECUENCIA SQL
    const sql = "INSERT INTO usuarios SET ?"

    //OBJETO DE DATOS DEL NUEVO USUARIO
    const nuevoCliente = {
        imagen: req.body.imagen,
        nombre: req.body.nombre,
        apellidos: req.body.apellidos,
        dni: req.body.dni,
        email: req.body.email,
        contrasenna: encryptedPassword,
        direccion: req.body.direccion,
        ciudad: req.body.ciudad,
        provincia: req.body.provincia,
        codigo_postal: req.body.codigo_postal,
        pais: req.body.pais,
        telefono: req.body.telefono,
        rol: 'normal'
    }

    //CONEXION A LA BBDD
    connection.query( sql, nuevoCliente, error => {
        if(error) throw error
        console.log('Usuario creado correctamente');
      })
    
    //ENVIO DE EMAIL DE CONFIRMACION
    const output = `
    <h1>User successfuly registered!!!</h1>
    <h3>Your account has been activated</h3>
    <h3>Profile Details</h3>
    <ul>
    <li>Image: ${req.body.imagen}</li>
    <li>Name: ${req.body.nombre}</li>
    <li>Surname: ${req.body.surname}</li>
    <li>D.n.i: ${req.body.dni}</li>
    <li>Email: ${req.body.email}</li>
    <li>Address: ${req.body.direccion}</li>
    <li>City: ${req.body.ciudad}</li>
    <li>State: ${req.body.provincia}</li>
    <li>Zip Code: ${req.body.codigo_postal}</li>
    <li>Country: ${req.body.pais}</li>    
    <li>Phone: ${req.body.telefono}</li>
    </ul>
    `;
    async function main() {
        // Generate test SMTP service account from ethereal.email
        let testAccount = await nodemailer.createTestAccount();
      
        // create reusable transporter object using the default SMTP transport
        let transporter = await nodemailer.createTransport({
          host: "smtp.ethereal.email",
          port: 587,
          secure: false, // true for 465, false for other ports
          auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
          },
        });

    let info = await transporter.sendMail({
    from: 'info@galicianxperiences.com',
    to: req.body.email,
    subject: "User successfuly registered",
    text: 'Your account has been activated, thank you',
    html: output
    });
    console.log(info);
    console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

main().catch(console.error);

})

//AÑADIR ACTIVIDADES A LA BBDD
app.post('/actividades/add', (req, res) => {

    //SECUENCIA SQL
    const sql = 'INSERT INTO actividades SET ?'

    //OBJETO DE DATOS DE LA NUEVA ACTIVIDAD
    const newActivity = {
        imagen: req.body.imagen,
        tipo: req.body.tipo,
        nombre: req.body.nombre,
        lugar: req.body.lugar,
        descripcion: req.body.descripcion,
        duracion: req.body.duracion,
        plazas_disponibles: req.body.plazas_disponibles,
        fecha_inicio: req.body.fecha_inicio,
        fecha_fin: req.body.fecha_fin,
        precio: req.body.precio,
        alojamiento: req.body.alojamiento
    
    }
        //CONEXION A LA BBDD
        connection.query( sql, newActivity, error => {
          if(error) throw error
          console.log('Actividad creada correctamente');
        })
});

//AÑADIR VALORACION ACTIVIDAD A LA BBDD
app.post('/usuarios/rating', (req, res) => {

    //SECUENCIA SQL
    const sql = 'INSERT INTO rating SET ?'

    //OBJETO DE DATOS DE LA NUEVA VALORACION
    const newRating = {
        fecha_actividades: req.body.fecha_actividades,
        actividades_realizadas: req.body.actividades_realizadas,
        comentario: req.body.comentario,
        valoracion: req.body.valoracion
        }
        //CONEXION A LA BBDD
        connection.query( sql, newRating, error => {
          if(error) throw error
          console.log('Valoracion enviada correctamente');
        })
});

//AÑADIR RESERVAS A LA BBDD
app.post('/actividades/booking/:id', (req, res) => {

    //SECUENCIA SQL
    const sql = 'INSERT INTO reservas SET ?'

    //OBJETO DE DATOS DE LA NUEVA RESERVA
    const newBooking = {
        id_actividad: req.body.id_actividad,
        nombre_actividad: req.body.nombre_actividad,
        descripcion: req.body.descripcion,
        imagen: req.body.imagen,
        id_usuario: req.body.id_usuario,
        nombre_usuario: req.body.nombre_usuario,
        email: req.body.email,
        fecha: req.body.fecha,
        numero_asistentes: req.body.numero_asistentes
        }
        console.log(newBooking);
        //CONEXION A LA BBDD
        connection.query( sql, newBooking, error => {
          if(error) throw error
          console.log('Reserva realizada correctamente');
        })

         //ENVIO DE EMAIL DE CONFIRMACION
        const output = `
        <h1>Your booking is confirmed!!!</h1>
        <h3>Booking Details</h3>
        <ul>
        <li>Activity Booked: ${req.body.nombre_actividad}</li>
        <li>Description: ${req.body.descripcion}</li>
        <li>Booking Day: ${req.body.fecha}</li>
        <li>Name: ${req.body.nombre_usuario}</li>
        <li>Email: ${req.body.email}</li>
        <li>Number of assistants: ${req.body.numero_asistentes}</li>
        </ul>
        `;
        async function main() {
            // Generate test SMTP service account from ethereal.email
            let testAccount = await nodemailer.createTestAccount();
        
            // create reusable transporter object using the default SMTP transport
            let transporter = await nodemailer.createTransport({
            host: "smtp.ethereal.email",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: testAccount.user, // generated ethereal user
                pass: testAccount.pass, // generated ethereal password
            },
            });

        let info = await transporter.sendMail({
        from: 'info@galicianxperiences.com',
        to: req.body.email,
        subject: "Your booking is confirmed!!!",
        text: 'Your booking is confirmed!!!',
        html: output
        });
        console.log(info);
        console.log("Message sent: %s", info.messageId);
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

        // Preview only available when sending through an Ethereal account
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        }

        main().catch(console.error);

});

//ACTUALIZANDO USUARIOS EN LA BBDD
app.put('/usuarios/update/:id', (req, res) => {

    //DATOS QUE RECIBIMOS
    const id = req.params.id
    const imagen = req.body.imagen
    const nombre = req.body.nombre
    const apellidos = req.body.apellidos
    const dni = req.body.dni
    const email = req.body.email
    const direccion = req.body.direccion
    const ciudad = req.body.ciudad
    const provincia = req.body.provincia
    const codigo_postal = req.body.codigo_postal
    const pais = req.body.pais
    const telefono = req.body.telefono
    const rol = req.body.rol     

    //SECUENCIA SQL
    const sql = `UPDATE usuarios SET imagen='${imagen}', nombre='${nombre}', apellidos='${apellidos}', dni='${dni}', email='${email}', direccion='${direccion}', ciudad='${ciudad}', provincia='${provincia}', codigo_postal='${codigo_postal}', pais='${pais}', telefono='${telefono}', rol='${rol}' WHERE id='${id}'`;  

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Usuario actualizado con exito');
    })
})

//ACTUALIZANDO ACTIVIDADES EN LA BBDD
app.put('/actividades/update/:id', (req, res) => {

    //DATOS QUE RECIBIMOS
    const id = req.params.id
    const imagen = req.body.imagen
    const tipo = req.body.tipo
    const nombre = req.body.nombre
    const lugar = req.body.lugar
    const descripcion = req.body.descripcion
    const duracion = req.body.duracion
    const plazas_disponibles = req.body.plazas_disponibles
    const fecha_inicio = req.body.fecha_inicio
    const fecha_fin = req.body.fecha_fin
    const precio = req.body.precio
    const alojamiento = req.body.alojamiento
    //SECUENCIA SQL
    const sql = `UPDATE actividades SET imagen='${imagen}', tipo='${tipo}', nombre='${nombre}', 
    lugar='${lugar}', descripcion='${descripcion}', duracion='${duracion}', plazas_disponibles='${plazas_disponibles}', 
    fecha_inicio='${fecha_inicio}', fecha_fin='${fecha_fin}', precio='${precio}', alojamiento='${alojamiento}' 
    WHERE id='${id}'`;

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Actividad actualizada con exito');
    })
})

//ACTUALIZANDO RESERVAS EN LA BBDD
app.put('/bookings/update/:id', (req, res) => {

    //DATOS QUE RECIBIMOS
    const id = req.params.id
    const id_actividad = req.body.id_actividad
    const nombre_actividad = req.body.nombre_actividad
    const descripcion = req.body.descripcion
    const imagen = req.body.imagen
    const id_usuario = req.body.id_usuario
    const nombre_usuario = req.body.nombre_usuario
    const email = req.body.email
    const fecha = req.body.fecha
    const numero_asistentes = req.body.numero_asistentes
    
    //SECUENCIA SQL
    const sql = `UPDATE reservas SET id_actividad='${id_actividad}', nombre_actividad='${nombre_actividad}', descripcion='${descripcion}', imagen='${imagen}', id_usuario='${id_usuario}', 
    nombre_usuario='${nombre_usuario}', email='${email}', fecha='${fecha}', numero_asistentes='${numero_asistentes}' 
    WHERE id='${id}'`;

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Reserva actualizada con exito');
    })
})

//BORRANDO USUARIOS EN LA BBDD
app.delete('/usuarios/delete/:id', (req, res) => {

    //DATOS QUE LLEGAN DE LA VISTA
    const id = req.params.id

    //SECUENCIA SQL
    const sql = `DELETE FROM usuarios WHERE id='${id}'`;

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Usuario eliminado con exito');
    })
})

//BORRANDO RESERVAS EN LA BBDD
app.delete('/bookings/delete/:id', (req, res) => {

    //DATOS QUE LLEGAN DE LA VISTA
    const id = req.params.id

    //SECUENCIA SQL
    const sql = `DELETE FROM reservas WHERE id='${id}'`;

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Reserva eliminada con exito');
    })
})

//BORRANDO ACTIVIDADES EN LA BBDD
app.delete('/actividades/delete/:id', (req, res) => {

    //DATOS QUE LLEGAN DE LA VISTA
    const id = req.params.id

    //SECUENCIA SQL
    const sql = `DELETE FROM actividades WHERE id='${id}'`;

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Actividad eliminada con exito');
    })
})

//ENVIO FORMULARIO DE CONTACTO
app.post('/form/send/', (req, res) => {
    const output = `
    <p>You have a new contact request</p>
    <h3>Contact Details</h3>
    <ul>
    <li>Name: ${req.body.name}</li>
    <li>Company: ${req.body.company}</li>
    <li>Email: ${req.body.email}</li>
    <li>Phone: ${req.body.phone}</li>
    </ul>
    <h3>Message:</h3>
    <p>${req.body.message}</p>
    `;
    
// async..await is not allowed in global scope, must use a wrapper
async function main() {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

    let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
        },
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: req.body.email, // sender address
        to: "info@galicianxperiences.com", // list of receivers
        subject: "Galician Xperiences Contact Request", // Subject line
        text: "Hello world?", // plain text body
        html: output // html body
    });
  
  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

}

main().catch(console.error);

});

//AUTH DE LA API - LOGIN
app.post('/auth', async (req, res) => {
        
    //DATOS QUE RECIBO
    const email = req.body.email
    const password = req.body.password;
   
    //SECUENCIA SQL - BUSCAMOS AL USUARIO CON ESE EMAIL
    const sql = `SELECT * FROM usuarios WHERE email = '${email}'`
    
    //CONEXION A LA BBDD
    const connection = await database.connection();
    const [results] = await connection.execute(sql, [email, password])
    let description;
    let code;
    try {
        //SI NO EXISTE EL EMAIL EN LA BBDD DEVOLVEMOS ERROR
        if (results[0].length = 0) {
        res.send(code = 400), (description = "Email incorrecto");
        } else {
        //COMPARAMOS LAS PASSWORD
        const match = await bcrypt.compare(password, results[0].contrasenna)
            console.log(match);
            //SI NO COINCIDEN DEVOLVEMOS ERROR
            if (match === false)  {
            console.log('Password incorrecta');
            (code = 400), (description = "Contraseña incorrecta");
            } else {
                (code = 200), (description = "Login correcto");
            }
            console.log(code, description);  
        }
            //SI LAS PASSWORD COINCIDEN SEGUIMOS ADELANTE
            if(code === 200) {
                const payload = {
                    check: true
                }
                //GUARDANDO SI ES ADMIN O NO
                let admin = null
                if(results[0].rol === 'admin') {
                    admin = true
                } else {
                    admin = false
                }
                //GUARDANDO NOMBRE, APELLIDOS E IMAGEN DEL USUARIO
                    let nombre = ''
                    let apellidos = ''
                    let imagen = ''
                    let id = ''
                    nombre = results[0].nombre
                    apellidos = results[0].apellidos
                    imagen = results[0].imagen
                    id = results[0].id
                //TOKEN
                const token = jwt.sign(payload, app.get('llave'), {
                    expiresIn: '1 day'
                })
                //DATOS QUE ENVIAMOS PARA SER GUARDADOS EN EL LOCALSTORAGE
                res.status(200).json({
                    mensaje: 'Autenticacion completeda con exito',
                    token: token,
                    rol: admin,
                    nombre: nombre,
                    apellidos: apellidos,
                    imagen: imagen,
                    id: id
                })
                console.log('datos ok');
                console.log(admin);
                console.log(results[0].rol);
                console.log(results[0].nombre);
                console.log(results[0].apellidos);
                console.log(results[0].imagen);
                console.log(results[0].id);
            } else {
                console.log('Datos incorrectos');
                res
          .status(404)
          .send(
            "Los parametros introducidos son incorrectos o inexistentes, se ha producido un error en el login");
            }
    } catch (e) {
       
        res
          .status(404)
          .send(
            "Los parametros introducidos son incorrectos o inexistentes, se ha producido un error en el login");
        }
});
