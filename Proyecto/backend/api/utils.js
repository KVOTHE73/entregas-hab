//IMPORTAMOS LIBRERIAS
import jwt from 'jwt-decode'
import axios from 'axios'

const ENDPOINT = 'http://localhost:3000'

//FUNCION DE LOGIN
export function login(email, password) { 
   
    const user = { email, password };
        let error = 'Looks like there is an error in your email or your password, please try again.'
        return axios.post(`${ENDPOINT}/auth`, user)
        .then(function(response) {
            console.log(response)
            //ME GUARDO EL TOKEN
            setAuthToken(response.data.token)
            //ME GUARDO EL ROL
            setIsAdmin(response.data.rol)
            //ME GUARDO EL NOMBRE DEL USUARIO
            setName(response.data.nombre)
            //ME GUARDO EL APELLIDO DEL USUARIO
            setSurname(response.data.apellidos)
            //ME GUARDO LA IMAGEN DEL USUARIO
            setPic(response.data.imagen)
            //ME GUARDO EL ID DEL USUARIO
            setId(response.data.id)
        })
        .catch(function(response) {
        console.log('Looks like there is an error in your email or your password, please try again.');
        throw alert(error)
    })
}

//FUNCION PARA GUARDAR EN LOCALSTORAGE EL JSONWEBTOKEN
export function setAuthToken(token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    localStorage.setItem('AUTH_TOKEN_KEY', token)
}

//FUNCION PARA RECUPERAR EL TOKEN DESDE LOCALSTORAGE
export function getAuthToken() {
    return localStorage.getItem('AUTH_TOKEN_KEY')
}

//FUNCION PARA CONSEGUIR LA FECHA DE CADUCIDAD DEL TOKEN
export function tokenExpiration(encodedToken) {
    let token = jwt(encodedToken)
    if(!token.exp) {
        return null
    }
    let date = new Date(0)
    date.setUTCSeconds(token.exp)
    return date
}

//FUNCION QUE COMPRUEBA SI EL TOKEN ESTA CADUCADO
export function isExpired(token) {
    let expirationDate = tokenExpiration(token)
    return expirationDate < new Date()
}

//FUNCION QUE COMPRUEBA SI LA PERSONA ESTA LOGUEADA Y SU TOKEN ES VALIDO
export function isLoggedIn() {
    let authToken = getAuthToken()
    return !!authToken && !isExpired(authToken)
}

//FUNCION PARA GUARDAR ADMIN EN LOCALSTORAGE
export function setIsAdmin(rol) {
    localStorage.setItem('ROLE', rol)
}

//FUNCION PARA RECUPERAR EL ADMIN DE LOCALSTORAGE
export function getIsAdmin() {
    return localStorage.getItem('ROLE')
}

//FUNCION PARA SABER SI ES ADMIN O NO
export function checkIsAdmin() {
    let role = null
    let admin = getIsAdmin()

    if(admin === 'true') {
        role = true
    } else {
        role = false
    }
    return role
}

//FUNCION DE GUARDAR EL NOMBRE DE USER EN LOCALSTORAGE
export function setName(nombre) {
    localStorage.setItem('NAME', nombre)
}

//FUNCION DE RECUPERAR EL NOMBRE DEL USUARIO EN LOCALSTORAGE
export function getName() {
    return localStorage.getItem('NAME')
}

//FUNCION DE GUARDAR EL APELLIDO DEL USUARIO EN LOCALSTORAGE
export function setSurname(apellidos) {
    localStorage.setItem('SURNAME', apellidos)
}

//FUNCION DE RECUPERAR EL APELLIDO DEL USUARIO EN LOCALSTORAGE
export function getSurname() {
    return localStorage.getItem('SURNAME')
}

//FUNCION DE GUARDAR LA IMAGEN DEL USUARIO EN LOCALSTORAGE
export function setPic(imagen) {
    localStorage.setItem('PIC', imagen)
}

//FUNCION DE RECUPERAR LA IMAGEN DEL USUARIO EN LOCALSTORAGE
export function getPic() {
    return localStorage.getItem('PIC')
}

//FUNCION DE GUARDAR EL ID DEL USUARIO EN LOCALSTORAGE
export function setId(id) {
    localStorage.setItem('ID', id)
}

//FUNCION DE RECUPERAR EL ID DEL USUARIO EN LOCALSTORAGE
export function getId() {
    return localStorage.getItem('ID')
}

//FUNCION DE LOGOUT
export function logout() {
    axios.defaults.headers.common['Authorization'] = ''
    localStorage.removeItem('AUTH_TOKEN_KEY')
    localStorage.removeItem('ROLE')
    localStorage.removeItem('NAME')
    localStorage.removeItem('SURNAME')
    localStorage.removeItem('PIC')
    localStorage.removeItem('ID')
}


