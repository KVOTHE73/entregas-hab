const mysql = require("mysql2/promise");

async function connection() {
  return await mysql.createConnection({
    host: "localhost",
    user: "proyecto",
    password: "1234",
    database: "proyect_hab_database",
  });
}

module.exports = {
  connection,
};
