-- usuarios (#id, nombre, apellidos, dni, email, password, direccion, ciudad, provincia, codigo_postal, pais, telefono, rol, -id_actividad);
-- actividades (#id, tipo, nombre, lugar, descripcion, imagen, duracion, plazas, fecha_inicio, fecha_fin, precio, alojamiento);
-- reservas (#id, fecha, numero_asistentes);
-- pagos (#id, numero_tarjeta_credito, nombre_tarjeta_credito, caducidad_tarjeta_credito, cnv_tarjeta_credito);
-- rating (#id, fecha_actividades, actividades_realizadas, comentario, valoracion);
-- usuarios_reservas (#(id_usuario, id_reserva));
-- reservas_actividades (#(id_reserva, id_actividad));
-- pagos_reservas (#(id_pago, id_reserva));

USE proyect_hab_database;

SET FOREIGN_KEY_CHECKS = 0; 

CREATE TABLE IF NOT EXISTS usuarios (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    imagen VARCHAR(500),
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    dni VARCHAR(9) UNIQUE NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL,
    contrasenna VARCHAR(512) UNIQUE NOT NULL,
    direccion VARCHAR (500),
    ciudad VARCHAR (25),
    provincia VARCHAR (25),
    codigo_postal VARCHAR (10),
    pais VARCHAR (25),
    telefono VARCHAR(50),
    rol VARCHAR (15),
    id_actividad INT UNSIGNED,
    FOREIGN KEY (id_actividad) REFERENCES actividades(id)
);

CREATE TABLE IF NOT EXISTS actividades (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    tipo VARCHAR(15) NOT NULL,
    nombre VARCHAR(50) UNIQUE NOT NULL,
    lugar VARCHAR(50),
    descripcion VARCHAR(500),
    imagen VARCHAR(500),
    duracion VARCHAR(15),
    plazas_disponibles VARCHAR (50),
    fecha_inicio DATE,
    fecha_fin DATE,
    precio VARCHAR (10),
    alojamiento VARCHAR (50)
);

CREATE TABLE IF NOT EXISTS reservas (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(50) UNIQUE NOT NULL,
    fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    numero_asistentes VARCHAR(10) NOT NULL,
    descripcion VARCHAR(500),
    imagen VARCHAR(500),
    id_usuario INT UNSIGNED,
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id),
    id_actividad INT UNSIGNED,
    FOREIGN KEY (id_actividad) REFERENCES actividades(id)
    
);

CREATE TABLE IF NOT EXISTS pagos (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    numero_tarjeta_credito VARCHAR (12) NOT NULL,
    nombre_tarjeta_credito VARCHAR(50) NOT NULL,
    caducidad_tarjeta_credito VARCHAR (5) NOT NULL,
    cnv_tarjeta_credito VARCHAR (3) NOT NULL
);

CREATE TABLE IF NOT EXISTS rating (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    fecha_actividades DATE NOT NULL,
    actividades_realizadas VARCHAR (50) NOT NULL,
    comentario VARCHAR (500),
    valoracion VARCHAR (15)
);

CREATE TABLE IF NOT EXISTS usuarios_reservas (
	id_usuario INT UNSIGNED,
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id),
    id_reserva INT UNSIGNED,
    FOREIGN KEY (id_reserva) REFERENCES reservas(id)
);

CREATE TABLE IF NOT EXISTS reservas_actividades (
	id_reserva INT UNSIGNED,
    FOREIGN KEY (id_reserva) REFERENCES reservas(id),
    id_actividad INT UNSIGNED,
    FOREIGN KEY (id_actividad) REFERENCES actividades(id)
);

CREATE TABLE IF NOT EXISTS pagos_reservas (
	id_pago INT UNSIGNED,
    FOREIGN KEY (id_pago) REFERENCES pagos(id),
    id_reserva INT UNSIGNED,
    FOREIGN KEY (id_reserva) REFERENCES reservas(id)
);

SET FOREIGN_KEY_CHECKS = 1;
