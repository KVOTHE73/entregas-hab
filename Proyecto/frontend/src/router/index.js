import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import { isLoggedIn, checkIsAdmin } from '/home/hab8/Documents/HackaBoss/Entregas/entregas-hab/Proyecto/backend/api/utils.js'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/contact',
    name: 'Contact',
    component: () => import('../views/Contact.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/activities',
    name: 'Activities',
    component: () => import('../views/Activities.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/Register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/user_dashboard',
    name: 'UserDashboard',
    component: () => import('../views/UserDashboard.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: false
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/user_dashboard',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/activity_register',
    name: 'ActivityRegister',
    component: () => import('../views/AdministrationViews/ActivityRegister.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: true
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/activity_update',
    name: 'ActivitiesUpdate',
    component: () => import('../views/AdministrationViews/ActivitiesUpdate.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: true
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/bookings',
    name: 'ManageBookings',
    component: () => import('../views/AdministrationViews/ManageBookings.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: true
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/ratings',
    name: 'ManageUsersRatings',
    component: () => import('../views/AdministrationViews/ManageUsersRatings.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: true
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/users_updateordelete',
    name: 'UsersUpdate',
    component: () => import('../views/AdministrationViews/UsersUpdateDelete.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: true
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/user_bookings',
    name: 'UserBookings',
    component: () => import('../views/UserViews/UserBookings.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: false
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/user_rating',
    name: 'UserRatingXperiences',
    component: () => import('../views/UserViews/UserRatingXperiences.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: false
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '/user_dashboard/user_profile',
    name: 'UserUpdate',
    component: () => import('../views/UserViews/UserUpdateDelete.vue'),
    meta: {
      allowAnon: false,
      onlyAdmin: false
    },
    //APPGUARD QUE COMPRUEBA SI SE ES ADMIN PARA PERMITIR O NO EL ACCESO A LA RUTA
    beforeEnter: (to, from, next) => {
      if(to.meta.onlyAdmin === true && !checkIsAdmin()) {
        next({
          path: '/activities',
          query: {redirect: to.fullPath}
        })
      }  else {
          next()
      }
    }
  },
  {
    path: '*',
    name: 'Error',
    component: () => import('../views/Error.vue'),
    meta: {
      allowAnon: true
    }
  }
]

const router = new VueRouter({
  routes
})

//APPGUARD QUE PROTEGE LAS RUTAS EN FUNCION DE SI SE PERMITE ACCESO 
//PUBLICO (ALLOWANON: TRUE) O PRIVADO (ALLOWANON: FALSE) 
router.beforeEach( (to, from, next) => {
  if(!to.meta.allowAnon && !isLoggedIn()) {
    next ({
      path: '/activities',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
})

export default router
