import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.use(require('vue-moment'));


//IMPORTAMOS FONTAWESOME, LIBRERIA DE ICONOS
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret)
Vue.component('font-awesome-icon', FontAwesomeIcon)

//IMPORTAMOS VUEHEADFUL, LIBRERIA QUE PONE NOMBRE A CADA PESTAÑA
import vueHeadful from 'vue-headful'

Vue.component('vue-headful', vueHeadful)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
