const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// just for academic purpose until database module is ready
const bd = require('./bd_mock');

// buscar email en la bbdd
// nos devolverá un JSON para el usuario con su password.
// Al ser un usuario unico no necesitamos definir un rol.

const login = async (req, res) => {
    const { email, password } = req.body;
   
    const user = bd.usuarioUnico(email);

    if (!user) {
        res.status(404).send(); //si el email introducido no coincide
        return;                 //con el de nuestra base de datos
    }

    // comprobamos la password
    const validezPassword = await bcrypt.compare(password, user.password);
    //bcrypt compara las passwords hasheadas

    if (!validezPassword) {
        res.status(401).send(); //si la password introducida no coincide
        return;
    }

    // generamos el token
    const tokenPayload = { id: user }; 
    const token = jwt.sign(tokenPayload, process.env.SECRET, { 
        expiresIn: '1d' 
    });

    res.json({
        token
    })
}

module.exports = {
    login
}