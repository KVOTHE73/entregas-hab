let articulos = [];

const listarArticulos = (req, res) => {
    
    let filtrarArticulos = articulos;

    const nombre = req.query['nombre'];

    // if (nombre === undefined) {
    //     res.status(404).send('Error: Por favor introduzca un nombre de producto válido');
    //     return;
    //   }
    
    

    if (nombre !== undefined) {
      filtrarArticulos = articulos.filter(articulo => nombre.indexOf(articulo.nombre) !== -1)
    }

    // if (filtrarArticulos !== articulos) {
    //     res.status(404).send('El articulo buscado no existe');
    //     return;
    //   }

    res.json(filtrarArticulos);
}

const addArticulos = (req, res) => {
    
    articulos.push({
        nombre: req.body.nombre.toLowerCase(),
        stock: req.body.stock,
        precio: req.body.precio
    })

    res.send();
}

const actualizarArticulos = (req, res) => {
    let buscarArticulo = articulos.find(articulo => articulo.nombre === req.body.nombre);
    
    if (buscarArticulo === undefined) {
    res.status(404).send('El articulo buscado no existe');
    return;
  }

    buscarArticulo.nombre = req.body.nombre.toLowerCase(),
    buscarArticulo.stock = req.body.stock,
    buscarArticulo.precio = req.body.precio

    res.send()
};

module.exports = {

    listarArticulos,
    addArticulos,
    actualizarArticulos
}