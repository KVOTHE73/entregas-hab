/**
 * This file is just a double to use instead of real database.
 * 
 */

//Definimos al usuario unico

let usuario = [
    {
        email: 'semana3@hackaboss.com',
        password: '$2b$10$4naxdOuPjP8Qb3aHz3.DaOY7O0NWwc4oBru4D6ICdruPzGZ49U0B2'
    }   //Info para el corregidor: password hasheada resultado de 1234
];

const usuarioUnico = (email) => {
    const buscarEmail = user => user.email === email;

    return usuario.find( buscarEmail ); //devuelve el usuario con ese email 
}

module.exports = {
    usuarioUnico
};
