/**
 * Una tienda quiere ofrecer sus servicios online. 
 * Para ello se dispone a digitalizar su catálogo y mostrarlo 
 * en una web. Nos piden realizar la parte de backend, que debe
 * permitir añadir y modificar productos, para lo cual será 
 * necesario que el usuario esté autenticado; y permitirá también 
 * listar los productos existentes, que se podrá acceder 
 * libremente.
 * 
 * Notas:
 *   - no se pueden dar de alta usuarios. Deberá existir uno por
 * defecto para las tareas de administración.
 *   - la lista de productos puede llegar a ser muy grande, así 
 * que el usuario deberá poder filtrarla mediante parámetros
 * enviados en la `querystring`
 *   - la estructura de un producto es la siguiente:
 *       {
 *           name: '',
 *           stock: <número de productos disponibles de este modelo>
 *           precio: 100
 *       }
 * 
 * 
 * 
 */

require('dotenv').config(); //variables de entorno

const bodyParser = require('body-parser'); //creamos un req.body
const cors = require('cors'); 
const express = require('express');
const morgan = require('morgan');

const { login } = require('./controllers/usuario');
const { autenticacion } = require('./middlewares/autenticacion');
const { listarArticulos, addArticulos, actualizarArticulos } = require('./controllers/eventos');

const port = process.env.PORT;

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.use((req, res, next) => {
    console.log('código que se ejecuta antes de procesar el endpoint');
    next();
})

app.post('/usuario/login', login); //log del unico usuario registrado
app.post('/producto', autenticacion, addArticulos); //crear articulos nuevos
app.get('/producto', listarArticulos); //listar articulos - no requiere login
app.put('/producto', autenticacion, actualizarArticulos); //actualizar articulos

app.use((error, req, res, next) => {

    res
        .status(error.status || 500)
        .send({status: 'error', message: error.message})
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});

