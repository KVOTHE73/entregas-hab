require('dotenv').config();

const jwt = require('jsonwebtoken');

const autenticacion = (req, res, next) => {
    const { authorization } = req.headers;
    console.log(authorization);
    

    try {
        const decodedToken = jwt.verify(authorization, process.env.SECRET); //se comprueba el token
        req.auth = decodedToken;

    } catch(e) {
        const authError = new Error('invalid token');
        authError.status = 401;
        return next(authError);
    }

    next();
}

module.exports = {
    autenticacion
};

// require('dotenv').config();

// const jwt = require('jsonwebtoken');

// //autenticacion: comprobamos el token con jwt
// const autenticacion = (req, res, next) => {
//     const { authorization } = req.headers;

//     try {
//         const decodedToken = jwt.verify(authorization, process.env.SECRET);
//         req.auth = decodedToken;
//     } catch(e) {
//         const authError = new Error('invalid token');
//         authError.status = 401;
//         next(authError);
//     }
//     next();
// }

// module.exports = {

//     autenticacion
// };