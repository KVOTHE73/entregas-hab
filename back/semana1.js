/**
 * El objetivo de este ejercicio es implementar una aplicación de consola
 * para calcular la cuota mensual de un préstamo. Para ello, la aplicación debe:
 *     - usar una librería para realizar los cálculos necesarios (buscad en npmjs
 *       por las palabras loan, mortgage o payoff)
 *     - recibir por línea de comandos los argumentos necesarios para realizar los
 *       cálculos. Para esto podéis usar directamente la API de NodeJS (process.argv)
 *       o bien usar una librería como commander o yargs 
 *     - el resultado debe aparecer en la consola
 * 
 */

const mortgageCalculator = require("mortgage-calculator");
const process = require('process');
const argv = process.argv;
//TODO estaria muy bien aprender a usar commander para introducir los parametros por consola con estilo
const initialDeposit = parseInt(argv[2]);
const monthlyIncome = parseInt(argv[3]);
const interest = parseInt(argv[4]);
const term = parseInt(argv[5]);
const monthlyExpenses = parseInt(argv[6]);

const mortgage = mortgageCalculator.calculateMortgage({
    initialDeposit,
    monthlyIncome,
    interest,
    term,
    monthlyExpenses
})

if (argv.length < 7 || isNaN(initialDeposit)|| isNaN(monthlyIncome)|| isNaN(interest)|| isNaN(term)|| isNaN(monthlyExpenses)) {
    console.log('No se han introducido datos válidos para el cálculo. Por favor, revise los datos introducidos');
    
} else {

console.log(mortgage);

};






