/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 * 
 * El cliente necesitará una API REST para añadir eventos y poder obtener
 * una lista de los existentes.
 * 
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles 
 * son los código de error a devolver
 * 
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 * 
 */


const axiosCacheAdapter = require('axios-cache-adapter');
const bodyParser = require('body-parser');
const express = require('express');
const axios = require('axios');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function (req, res, next){
    console.log(`${new Date()} - Peticion recibida`)
    next();
})

const cache = axiosCacheAdapter.setupCache({
    maxAge: 30 * 1000
  });
  
const cachedAxios = axios.create({
    adapter: cache.adapter
});

let events = [];
// let events = {
//     'teatro': [],
//     'concierto': [],
//     'monologo': []
//   };

app.get('/:eventos', (req, res) => {
    //res.json(Object.keys(events));
    res.json(events);
})

let globalId = 0;

app.post('/:eventos', (req, res) => {
    const tipoEvento = req.body.tipo;

// Error 400 si quieren crear una colección distinta de las tres permitidas
  if (tipoEvento.toLowerCase() !== 'teatro' && tipoEvento.toLowerCase() !== 'concierto' 
  && tipoEvento.toLowerCase() !== 'monologo') {
     res.status(400).send();
     return;
  }  

// // Error 409 si ya existe una colección con ese nombre
//   if (events[tipoEvento.toLowerCase()] !== undefined) { 
//      events[tipoEvento.toLowerCase()] = [];
//      res.status(409).send(); 
//      return;
//   }

    let datos = {
        id: globalId++,
        tipo: req.body.tipo,
        nombre: req.body.nombre,
        aforo: req.body.aforo,
        artista: req.body.artista,
    }
   
    events.push(datos);
    //events[tipoEvento].push(datos);
    res.send();
    
});

app.listen(3000);