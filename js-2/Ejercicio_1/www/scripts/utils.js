//variables

const bookButtons = document.querySelectorAll('.bookButton')
const cart = document.querySelector('.list-card')
const deleteAllCart = document.querySelector('#delete-cart')
const deleteCart = document.querySelector('.list-card')
const aside = document.getElementById('reservas')

aside.style.display = 'none'

// funciones

const getCart = list => {
    cart.innerHTML = ''
    list.forEach(hotel => bookInsert(hotel))
}

const bookInsert = hotel => {
    const card = document.createElement('div')
    card.id = 'cart-card-' + hotel.id
    card.className = 'cart-card'
    card.innerHTML = `
        <img src="${hotel.img}" alt="${hotel.name} imagen">
        <section>
            <h3 class="name">${hotel.name}</h3>
            <h2 class="price">${hotel.price}</h2>
            <button class="delete-card" id="delete" data-id="${hotel.id}">DELETE</button>
        </section>
        
    `
    if (cart.getElementsByTagName('div').length >= 0) {
        aside.style.display = 'block'
    }
    cart.appendChild(card)
}

const addToLocalStorage = hotel => {
    const hotelsList  = JSON.parse(localStorage.getItem('reservas')) || []
    const newHotelsList = [...hotelsList, hotel]
    localStorage.setItem('reservas', JSON.stringify(newHotelsList))
    getCart(newHotelsList)
}

const removeFromLocalStorage = id => {
   
    const hotelsList  = JSON.parse(localStorage.getItem('reservas')) || []
    const newHotelsList = hotelsList.filter((hotel)=> hotel.id !== id)
    localStorage.setItem('reservas', JSON.stringify(newHotelsList))
    getCart(newHotelsList)

    if (newHotelsList.length<=0) {
        aside.style.display = 'none'
    }
}

const removeAllFromLocalStorage = () => {
    localStorage.clear()
    aside.style.display = 'none'
    const hotelsList  = JSON.parse(localStorage.getItem('reservas')) || []
    getCart(hotelsList)
}

const hotelData = hotel => {
    const hotelInfo = {
        img: hotel.querySelector('img').src,
        name: hotel.querySelector('h3').textContent,
        price: hotel.querySelector('h2').textContent,
        id: hotel.getAttribute('data-id')
    }
    return hotelInfo
}

const booking = e => {
    e.preventDefault()
    const hotel = hotelData(e.target.parentElement)
    addToLocalStorage(hotel)
}

const deleteBooking = e => {
    e.preventDefault()
    let elemento = e.target.innerText
    console.log(elemento);
    if (elemento = 'DELETE') { 
    removeFromLocalStorage(e.target.getAttribute('data-id'))
    }
}

export {bookButtons, deleteAllCart, deleteCart, deleteBooking, booking, removeAllFromLocalStorage}



