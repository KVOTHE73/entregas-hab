
import {deleteAllCart, deleteCart, deleteBooking, bookButtons, booking, removeAllFromLocalStorage} from './utils.js'

// listeners

deleteAllCart.addEventListener('click', () => removeAllFromLocalStorage())

bookButtons.forEach(button => {
    button.addEventListener('click', e => booking(e))
})

deleteCart.addEventListener('click', deleteBooking)