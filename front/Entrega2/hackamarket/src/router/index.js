//AQUI SE ESPECIFICA CADA RUTA, CADA PAGINA TIENE QUE TENER AQUI LA SUYA

//IMPORTAMOS LAS LIBRERIAS NECESARIAS
import Vue from 'vue'
import VueRouter from 'vue-router'
//HAY DOS FORMAS DE IMPORTAR, EN ESTA LA PAGINA SE CARGARA AL ENTRAR A LA WEB Y ESTARA SIEMPRE DISPONIBLE.
//ESTO CONSUME RECURSOS TAL VEZ DE FORMA INNECERSARIA SI LAS PAGINAS TIENEN MUCHA CARGA Y/O HAY MUCHAS
import Home from '../views/Home.vue'

Vue.use(VueRouter)

//SE ESTANLECE UN PATH QUE SERA LA QUERYSTRING A LA PAGINA, SU NOMBRE Y SE IMPORTA SU RUTA LOCAL A ELLA
  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    //ESTA ES LA SEGUNDA FORMA DE IMPORTAR. DE ESTA FORMA LA PAGINA SE CARGARA SOLO AL ACCEDER A ELLA
    component: () => import('../views/About.vue')
  },
  {
    path: '/productos',
    name: 'Productos',
    component: () => import('../views/Productos.vue')
  },
  {
    path: '/clientes',
    name: 'Clientes',
    component: () => import('../views/Clientes.vue')
  },
  {
    path: '/registrar-cliente',
    name: 'RegistrarCliente',
    component: () => import('../views/RegistrarCliente.vue')
  },
  {
    path: '/registrar-producto',
    name: 'RegistrarProducto',
    component: () => import('../views/RegistrarProducto.vue')
  },
  //ASI SE DEFINE LA RUTA DE LA PAGINA DE ERROR. EL * INDICA CUALQUIER RUTA NO DEFINIDA
  {
    path: '*',
    name: 'Error',
    component: () => import('../views/Error.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
