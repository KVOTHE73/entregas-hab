const express = require('express')
const cors = require('cors')
const bodyparser = require('body-parser')
const mysql = require('mysql')
const app = express()

app.use(cors())
app.use(bodyparser.urlencoded ( { extended: true } ) )
app.use(bodyparser.json())

const connection = mysql.createConnection( {
    host: 'localhost',
    user: 'vuejs',
    password: 'vuejs',
    database: 'vue2'
})

connection.connect(error => {
    if(error) throw error
    console.log('Database up!⏫');
})

const PORT = 3050

app.listen(PORT, () => console.log('Api up!🙃'));

//RECOGER TODOS LOS CLIENTES DE LA BBDD
app.get('/clientes', (req, res) => {

    //SECUENCIA SQL
    const sql = 'SELECT * FROM listaclientes'

    //CONEXION A LA BBDD
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
            console.log('No existen clientes que mostrar');
        }
    })
})

//RECOGER TODOS LOS PRODUCTOS DE LA BBDD
app.get('/productos', (req, res) => {

    //SECUENCIA SQL
    const sql = 'SELECT * FROM listaproductos'

    //CONEXION A LA BBDD
    connection.query( sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        } else {
            console.log('No existen productos que mostrar');
        }
    })
})

//AÑADIR CLIENTES A LA BBDD
app.post('/clientes/add', (req, res) => {

    //SECUENCIA SQL
    const sql = 'INSERT INTO listaclientes SET ?'

    //OBJETO DE DATOS DEL CUEVO CLIENTE
    const nuevoCliente = {
        nombre: req.body.nombre,
        usuario: req.body.usuario,
        password: req.body.password,
        email: req.body.email,
        foto: req.body.foto
    }
    
    //CONEXION A LA BBDD
    connection.query( sql, nuevoCliente, error => {
        if(error) throw error
        console.log('Cliente creado correctamente');
    })
})
//AÑADIR PRODUCTOS A LA BBDD
app.post('/productos/add', (req, res) => {

    //SECUENCIA SQL
    const sql = 'INSERT INTO listaproductos SET ?'

    //OBJETO DE DATOS DEL CUEVO CLIENTE
    const nuevoProducto = {
        nombre: req.body.nombre,
        stock: req.body.stock,
        disponibilidad: req.body.disponibilidad,
        imagen: req.body.imagen
    }
    
    //CONEXION A LA BBDD
    connection.query( sql, nuevoProducto, error => {
        if(error) throw error
        console.log('Producto creado correctamente');
    })
})

//ACTUALIZANDO CLIENTES EN LA BBDD
app.put('/clientes/update/:id', (req, res) => {

    //DATOS QUE RECIBIMOS
    const id = req.params.id
    const nombre = req.body.nombre
    const usuario = req.body.usuario
    const password = req.body.password
    const email = req.body.email
    const foto = req.body.foto

    //SECUENCIA SQL
    const sql = `UPDATE listaclientes SET nombre='${nombre}', usuario='${usuario}', password='${password}', email='${email}',foto='${foto}' WHERE id='${id}'`;  

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Cliente actualizado con exito');
    })
})

//ACTUALIZANDO PRODUCTOS EN LA BBDD
app.put('/productos/update/:id', (req, res) => {

    //DATOS QUE RECIBIMOS
    const id = req.params.id
    const nombre = req.body.nombre
    const stock = req.body.stock
    const disponibilidad = req.body.disponibilidad
    const imagen = req.body.imagen

    //SECUENCIA SQL
    const sql = `UPDATE listaproductos SET nombre='${nombre}', stock='${stock}',disponibilidad='${disponibilidad}',imagen='${imagen}' WHERE id='${id}'`;  

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Producto actualizado con exito');
    })
})

//BORRANDO CLIENTES EN LA BBDD
app.delete('/clientes/delete/:id', (req, res) => {

    //DATOS QUE LLEGAN DE LA VISTA
    const id = req.params.id

    //SECUENCIA SQL
    const sql = `DELETE FROM listaclientes WHERE id='${id}'`;

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Cliente eliminado con exito');
    })
})

//BORRANDO PRODUCTOS EN LA BBDD
app.delete('/productos/delete/:id', (req, res) => {

    //DATOS QUE LLEGAN DE LA VISTA
    const id = req.params.id

    //SECUENCIA SQL
    const sql = `DELETE FROM listaproductos WHERE id='${id}'`;

    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Producto eliminado con exito');
    })
})