// https://jsonplaceholder.typicode.com/posts
// https://jsonplaceholder.typicode.com/posts/1

// a) Generar contador de mensajes por usario
// b) Generar una lista con la siguiente estructura:
/*[
    {
        userId: <userId>,
        posts: [
            {
                title: <title>
                body: <body>     // hay que obtenerlo de la segunda petición
            },
            {
                title: <title>
                body: <body>
            },
        ]
    }

]
*/

'use strict';

const axios = require('axios');

let idInterval;

async function dataFrom(){
    const dataSource = 'https://jsonplaceholder.typicode.com/posts';
  
    const Id = (datos => datos.id);
    const infoFromURL = id => axios.get(`${dataSource}/${id}`);
    let llamadas = await axios.get(dataSource);
   
    llamadas = llamadas.data
          .map(Id)
          .map(infoFromURL);

    const accessToForum= await Promise.all(llamadas);
 
    return accessToForum;

    
}

function contadorMensajesUsuario(informacion){
    let mensajesPorUsuario= {};
   // hacer reduce
    const contarMensajesPorUsuario = mensaje => {
        if (mensajesPorUsuario[`Usuario ${mensaje.data.userId}`] == undefined) {
            mensajesPorUsuario[`Usuario ${mensaje.data.userId}`] = 1;
        } 
        else{
            mensajesPorUsuario[`Usuario ${mensaje.data.userId}`] ++;
        }
    
    }
    informacion.forEach(contarMensajesPorUsuario)
    return mensajesPorUsuario
     
}

function listarMensajesPorUsuario(informacion){
    let mensajesPorUsuario= [];
    let usuariosDetectados = [];
    for (let mensaje of informacion) {
        //usar find --> funcion que devuelve true o false si encuentras en el índice.
        if (usuariosDetectados.indexOf(mensaje.data.userId)===-1){
            usuariosDetectados.push(mensaje.data.userId)
            mensajesPorUsuario.push({
                userId: mensaje.data.userId,
                post: [{
                    title: mensaje.data.title, 
                    body:mensaje.data.body}]
            })  
        }
        else {
        //    let temporal = []
        //    temporal = mensajesPorUsuario[usuariosDetectados.indexOf(mensaje.data.userId)].post
        //    temporal.push({title: mensaje.data.title, body:mensaje.data.body})
            mensajesPorUsuario[usuariosDetectados.indexOf(mensaje.data.userId)]['post']
                .push({
                    title: mensaje.data.title, 
                    body:  mensaje.data.body
                })
          
          //  } 
        }
      
    }
   
    return mensajesPorUsuario
}

dataFrom().then(datos =>{
    clearInterval(idInterval);
    console.log(contadorMensajesUsuario(datos));
    const prueba =  listarMensajesPorUsuario(datos)
    console.log(Object.values(prueba))
});

let i=1
const TIEMPOESPERA = 1000;
console.log(`Descargando archivos`)
idInterval = setInterval(function(){
    console.log(`${'*'.repeat(i)}`);
    i++;
},TIEMPOESPERA)
